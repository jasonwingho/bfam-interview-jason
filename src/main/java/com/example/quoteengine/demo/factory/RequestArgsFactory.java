package com.example.quoteengine.demo.factory;

import com.example.quoteengine.demo.pojo.RequestArgs;
import com.example.quoteengine.demo.utils.ParserUtils;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestArgsFactory {

    public static RequestArgs createRequestArgs (String[] args) {
        RequestArgs requestArgs = new RequestArgs();

        if (args.length != 3) {
            throw new IllegalArgumentException("Incorrect arguments");
        }

        requestArgs.setSecurityId(parseSecurityId(args[0]));
        requestArgs.setBuy(parseSide(args[1]));
        requestArgs.setQty(parseQty(args[2]));

        return requestArgs;
    }

    private static int parseSecurityId(String value) {
        AtomicInteger id = new AtomicInteger();

        if (!ParserUtils.tryParseInt(value, id)) {
            throw new NumberFormatException();
        }

        return id.get();
    }

    private static boolean parseSide(String value) {
        if (value.toUpperCase(Locale.ROOT).equals("BUY")) {
            return true;
        } else if (value.toUpperCase(Locale.ROOT).equals("SELL")) {
            return false;
        } else {
            throw new IllegalArgumentException("value must be BUY or SELL, current input = " + value);
        }
    }

    private static int parseQty(String value) {
        AtomicInteger qty = new AtomicInteger();

        if (!ParserUtils.tryParseInt(value, qty)) {
            throw new NumberFormatException();
        }

        return qty.get();
    }
}
