package com.example.quoteengine.demo.utils;

import java.util.concurrent.atomic.AtomicInteger;

public class ParserUtils {
    public static boolean tryParseInt(String value, AtomicInteger output) {
        try {
            output.set(Integer.parseInt(value));
        } catch (Exception ex) {
            return false;
        }

        return true;
    }
}
