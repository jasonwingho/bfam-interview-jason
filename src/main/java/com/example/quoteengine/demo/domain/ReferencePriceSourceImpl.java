package com.example.quoteengine.demo.domain;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.*;

@Slf4j
public class ReferencePriceSourceImpl implements ReferencePriceSource {
    private static final ReferencePriceSourceImpl INSTANCE = new ReferencePriceSourceImpl();

    private final ConcurrentHashMap<Integer, Double> priceMap;
    private final ConcurrentHashMap<Integer, LinkedList<ReferencePriceSourceListener>> listenerMap;

    public static ReferencePriceSourceImpl getInstance() {
        return INSTANCE;
    }

    private ReferencePriceSourceImpl() {
        this.priceMap = new ConcurrentHashMap<>();
        this.listenerMap = new ConcurrentHashMap<>();

        // Mock update every second
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(() -> {
            for (var entry: priceMap.entrySet()) {
                double spread = 0.05;

                if (Math.random() > 0.5) {
                    spread *= -1;
                }

                double newPrice = entry.getValue() + spread;

                // update price map
                priceMap.put(entry.getKey(), newPrice);

                // fire update
                if (listenerMap.containsKey(entry.getKey())) {
                    LinkedList<ReferencePriceSourceListener> listeners = listenerMap.get(entry.getKey());

                    while (!listeners.isEmpty()) {
                        listeners.poll().referencePriceChanged(entry.getKey(), newPrice);
                    }
                }
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        // Expect to be ReferencePriceSourceListenerImpl
        ReferencePriceSourceListenerImpl listenerImpl = (ReferencePriceSourceListenerImpl) listener;

        if (!this.listenerMap.containsKey(listenerImpl.getRequestArgs().getSecurityId())) {
            this.listenerMap.put(listenerImpl.getRequestArgs().getSecurityId(), new LinkedList<>());
        }

        this.listenerMap.get(listenerImpl.getRequestArgs().getSecurityId()).add(listenerImpl);

        if (!this.priceMap.containsKey(listenerImpl.getRequestArgs().getSecurityId())) {
            this.priceMap.put(listenerImpl.getRequestArgs().getSecurityId(), (double) ThreadLocalRandom.current().nextInt(1, 100));
        }
    }

    @Override
    public double get(int securityId) {
        // may null
        return priceMap.get(securityId);
    }

    public boolean isRequesting(int securityId) {
        return priceMap.containsKey(securityId);
    }
}
