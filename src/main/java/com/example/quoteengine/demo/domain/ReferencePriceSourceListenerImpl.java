package com.example.quoteengine.demo.domain;

import com.example.quoteengine.demo.pojo.RequestArgs;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import lombok.Getter;

public class ReferencePriceSourceListenerImpl implements ReferencePriceSourceListener {
    private final ChannelHandlerContext ctx;

    @Getter
    private RequestArgs requestArgs;

    public ReferencePriceSourceListenerImpl(ChannelHandlerContext ctx, RequestArgs args) {
        this.ctx = ctx;
        this.requestArgs = args;
    }

    @Override
    public void referencePriceChanged(int securityId, double price) {
        // get price from quote
        double quotePrice = QuoteCalculationEngineImpl.getInstance().calculateQuotePrice(securityId, price, this.requestArgs.isBuy(), this.requestArgs.getQty());

        if (this.ctx != null) {
            this.ctx.writeAndFlush(Unpooled.copiedBuffer(String.valueOf(quotePrice), CharsetUtil.UTF_8));
        }
    }
}
