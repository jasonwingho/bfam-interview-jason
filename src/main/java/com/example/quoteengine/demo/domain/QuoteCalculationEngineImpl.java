package com.example.quoteengine.demo.domain;

import com.example.quoteengine.demo.pojo.RequestArgs;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {
    private static final QuoteCalculationEngineImpl INSTANCE = new QuoteCalculationEngineImpl();

    private final double spread = 0.0025;

    public static QuoteCalculationEngineImpl getInstance() {
        return INSTANCE;
    }

    private QuoteCalculationEngineImpl() {
    }

    public void tryCalculateQuotePrice(ChannelHandlerContext ctx, RequestArgs requestArgs) {
        if (ReferencePriceSourceImpl.getInstance().isRequesting(requestArgs.getSecurityId())) {
            sendResponseToClient(ctx, requestArgs);
        } else {
            log.info("Security ID " + requestArgs.getSecurityId() + " is not requesting, make request");
            ReferencePriceSourceImpl.getInstance().subscribe(new ReferencePriceSourceListenerImpl(ctx, requestArgs));
        }
    }

    private void sendResponseToClient(ChannelHandlerContext ctx, RequestArgs requestArgs) {
        // can throw exception
        double refPrice = ReferencePriceSourceImpl.getInstance().get(requestArgs.securityId);
        double quotePrice = calculateQuotePrice(requestArgs.securityId, refPrice, requestArgs.isBuy, requestArgs.qty);
        ctx.writeAndFlush(Unpooled.copiedBuffer(String.valueOf(quotePrice), CharsetUtil.UTF_8));
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        double value = 0;

        if (buy) {
            value = referencePrice * quantity * (1d + spread);
        } else {
            value = referencePrice * quantity * (1d - spread);
        }

        log.info("Response to client with security ID = " + securityId
                + ", side = " + (buy? "BUY": "SELL")
                + ", qty = " + quantity
                + ", refprice = " + referencePrice
                + ", calculatedQuotePrice = " + value);

        return value;
    }
}
