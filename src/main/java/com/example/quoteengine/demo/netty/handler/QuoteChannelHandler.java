package com.example.quoteengine.demo.netty.handler;

import com.example.quoteengine.demo.domain.QuoteCalculationEngineImpl;
import com.example.quoteengine.demo.factory.RequestArgsFactory;
import com.example.quoteengine.demo.pojo.RequestArgs;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@ChannelHandler.Sharable
public class QuoteChannelHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) {
        try {
            ByteBuf byteBuf = (ByteBuf) obj;
            String requestString = byteBuf.toString(CharsetUtil.UTF_8).trim();

            log.info("Receive request from " + ctx.channel().remoteAddress() + " with args = " + requestString);

            String[] requests = requestString.split(" ");

            RequestArgs requestArgs = RequestArgsFactory.createRequestArgs(requests);

            // Will handle response inside
            QuoteCalculationEngineImpl.getInstance().tryCalculateQuotePrice(ctx, requestArgs);
        } catch (Exception ex) {
            log.error(ex.toString());

            // Response what's wrong to user
            ctx.writeAndFlush(ex.toString());
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error(cause.getMessage(), cause);
        ctx.writeAndFlush(cause.toString());
    }
}