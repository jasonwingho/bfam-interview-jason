package com.example.quoteengine.demo.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RequestArgs {
    public int securityId;

    public boolean isBuy;

    public int qty;
}
