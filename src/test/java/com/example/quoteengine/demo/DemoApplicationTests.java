package com.example.quoteengine.demo;

import com.example.quoteengine.demo.domain.QuoteCalculationEngineImpl;
import com.example.quoteengine.demo.factory.RequestArgsFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DemoApplicationTests {

	@Test
	public void testQuotePrice() {
		QuoteCalculationEngineImpl quoteCalculationEngine = QuoteCalculationEngineImpl.getInstance();
		double result = quoteCalculationEngine.calculateQuotePrice(123, 123.0, true, 100);
		double expected =  123 * 100 * (1d + 0.0025);
		assertEquals(result, expected);
	}

	@Test
	public void testRequestArgs() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			//Code under test
			String[] arrs = {"A", "BUY", "123"};
			RequestArgsFactory.createRequestArgs(arrs);
		});
	}

	@Test
	public void testIncorrectId() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			//Code under test
			String[] arrs = {"123", "NONBUY", "6756"};
			RequestArgsFactory.createRequestArgs(arrs);
		}, "value must be BUY or SELL, current input = NONBUY");
	}

	@Test
	public void testIllegaQuantityFormat() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			//Code under test
			String[] arrs = {"123", "BUY", "quantity"};
			RequestArgsFactory.createRequestArgs(arrs);
		});
	}

}
